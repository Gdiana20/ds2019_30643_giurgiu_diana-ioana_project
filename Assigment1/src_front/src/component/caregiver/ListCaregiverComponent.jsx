import React, { Component } from 'react'
import CaregiverApiService from "../../service/CaregiverApiService";

class ListCaregiverComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            caregivers: [],
           // patients:[],
            message: null
        }
       this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.reloadCaregiverList = this.reloadCaregiverList.bind(this);
        //this.addRelation= this.addRelation.bind(this);
        //this. reloadPatientList= this.reloadPatientList.bind(this);
          }

    componentDidMount() {
        this.reloadCaregiverList();
       // this.reloadPatientList();
    }



    reloadCaregiverList() {
        CaregiverApiService.fetchUser()
            .then((res) => {
                this.setState({caregivers: res.data.result})
            });
    }
 //   addRelation(id)
  //  {
   //   window.localStorage.setItem("CaregiverId",id);
    //  this.props.history.push('/caregiver_patient');
   // }

    deleteUser(userId) {
        CaregiverApiService.deleteUser(userId)
           .then(res => {
               this.setState({message : 'Caregiver deleted successfully.'});
               this.setState({caregivers: this.state.caregivers.filter(caregiver => caregiver.idCaregiver !== userId)});
           })

    }

    editUser(id) {
        window.localStorage.setItem("CaregiverId", id);
        this.props.history.push('/edit-caregiver');
    }

 // seePatients(id) {
   //     window.localStorage.setItem("CaregiverId", id);
    //    this.props.history.push('/seePatients');
    //}

    addUser() {
        window.localStorage.removeItem("CaregiverId");
        this.props.history.push('/add-caregiver');
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Caregiver Details</h2>
                <button className="btn btn-danger" style={{width:'100px'}} onClick={() => this.addUser()}> Add Caregiver</button>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="hidden">Id</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>caregiverEmail</th>
                            <th>caregiverGender</th>
                            <th>caregiverAddress</th>
                            <th>Patients</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.caregivers.map(
                        caregiver =>
                                    <tr key={caregiver.idCaregiver}>
                                        <td>{caregiver.caregiverName}</td>
                                        <td>{caregiver.caregiverSurname}</td>
                                        <td>{caregiver.caregiverEmail}</td>
                                        <td>{caregiver.caregiverGender}</td>
                                        <td>{caregiver.caregiverAddress}</td>
                                        <td>{caregiver.patientList.map(pat=>pat.patientName)}</td>
                                        <td>
                                            <button className="btn btn-success" onClick={() => this.deleteUser(caregiver.idCaregiver)}> Delete</button>
                                            <button className="btn btn-success" onClick={() => this.editUser(caregiver.idCaregiver)} style={{marginLeft: '20px'}}> Edit</button>
                                        </td>
                                    </tr>

                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }

}

export default ListCaregiverComponent;
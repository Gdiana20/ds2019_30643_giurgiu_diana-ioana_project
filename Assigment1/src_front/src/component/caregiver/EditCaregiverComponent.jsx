import React, { Component } from 'react'
import CaregiverApiService from "../../service/CaregiverApiService";

class EditCaregiverComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
             idCaregiver: '',
                        caregiverName: '',
                                   caregiverSurname: '',
                                   caregiverEmail: '',
                                   caregiverPassword: '',
                                   caregiverGender: '',
                                   caregiverAddress: '',
                                    patientList: '',
                                   message: null
        }
        this.saveCaregiver = this.saveCaregiver.bind(this);
        this.loadCaregiver = this.loadCaregiver.bind(this);
    }

    componentDidMount() {
        this.loadCaregiver();
    }

    loadCaregiver() {
        CaregiverApiService.fetchCaregiverByIdCaregiver(window.localStorage.getItem("idCaregiver"))
            .then((res) => {
                let user = res.data.result;
                this.setState({
                idCaregiver: user.idCaregiver,
                caregiverName: user.caregiverName,
                caregiverSurname: user.caregiverSurname,
                caregiverEmail: user.caregiverEmail,
                caregiverPassword: user.caregiverPassword,
                caregiverAddress: user.caregiverAddress,
                patientList: user.patientList,
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveCaregiver = (e) => {
        e.preventDefault();
        let user = {caregiverName: this.state.caregiverName, caregiverSurname: this.state.caregiverSurname, caregiverEmail: this.state.caregiverEmail, caregiverPassword: this.state.caregiverPassword, caregiverGender: this.state.caregiverGender, caregiverAddress: this.state.caregiverAddress,patientList:this.state.patientList};
        CaregiverApiService.editCaregiver(user)
            .then(res => {
                this.setState({message : 'Caregiver added successfully.'});
                this.props.history.push('/caregivers');
            });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit Caregiver</h2>
               <form>
                               <div className="form-group">
                                   <label>Name:</label>
                                   <input type="text" placeholder="caregiverName" name="caregiverName" className="form-control" value={this.state.caregiverName} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Surname:</label>
                                   <input type="caregiverSurname" placeholder="caregiverSurname" name="caregiverSurname" className="form-control" value={this.state.caregiverSurname} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Email:</label>
                                   <input placeholder="caregiverEmail" name="caregiverEmail" className="form-control" value={this.state.caregiverEmail} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Password:</label>
                                   <input placeholder="caregiverPassword" name="caregiverPassword" className="form-control" value={this.state.caregiverPassword} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Gender:</label>
                                   <input placeholder="caregiverGender" name="caregiverGender" className="form-control" value={this.state.caregiverGender} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Address:</label>
                                   <input placeholder="caregiverAddress" name="caregiverAddress" className="form-control" value={this.state.caregiverAddress} onChange={this.onChange}/>
                               </div>

                               <button className="btn btn-success" onClick={this.saveCaregiver}>Save</button>
                           </form>
            </div>
        );
    }
}

export default EditCaregiverComponent;
import React, { Component } from 'react'
import CaregiverApiService from "../../service/CaregiverApiService";

class AddRelationCaregiverPatient extends Component{

    constructor(props){
        super(props);
        this.state ={
        idPac:'',
        idCare:'',
                       message: null
        }
        this.getOne = this.getOne.bind(this);
    }

    getOne = (e) => {
        e.preventDefault();
        let rel = {idPac: this.state.idPac, idCare: this.state.idCare};
            console.log(rel);
       const { idPac, idCare } = this.state;
        console.log(idCare);
        CaregiverApiService.getOne(idPac,idCare);
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <h2 className="text-center">Add Caregiver</h2>
                <form>
                <div className="form-group">
                    <label>Caregiver Name:</label>
                    <input type= "number" placeholder="idPac" name="idPac" className="form-control" value={this.state.idPac} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Surname:</label>
                    <input type="number" placeholder="idCare" name="idCare" className="form-control" value={this.state.idCare} onChange={this.onChange}/>
                </div>
                <button className="btn btn-success" onClick={this.getOne}>Save</button>
            </form>
    </div>
        );
    }
}

export default AddRelationCaregiverPatient;
import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class ListUserComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            diseases: [],
            message: null
        }
        this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.reloadUserList = this.reloadUserList.bind(this);
    }

    componentDidMount() {
        this.reloadUserList();
    }

    reloadUserList() {
        ApiService.fetchUsers()
            .then((res) => {
                this.setState({diseases: res.data.result})
            });
    }

    deleteUser(userId) {
        ApiService.deleteUser(userId)
           .then(res => {
               this.setState({message : 'User deleted successfully.'});
               this.setState({diseases: this.state.diseases.filter(diseases => diseases.iddisease !== userId)});
           })

    }

    editUser(id) {
        window.localStorage.setItem("iddisease", id);
        this.props.history.push('/edit-disease');
    }

    addUser() {
        window.localStorage.removeItem("iddisease");
        this.props.history.push('/add-disease');
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Disease Details</h2>
                <button className="btn btn-danger" style={{width:'100px'}} onClick={() => this.addUser()}> Add User</button>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="hidden">Id</th>
                            <th>diseaseName</th>
                            <th>disease diagnostic date</th>
                            <th>disease simptoms</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.diseases.map(
                        user =>
                                    <tr key={user.iddisease}>
                                        <td>{user.diseaseName}</td>
                                        <td>{user.diseaseDiagnosticDate}</td>
                                        <td>{user.diseaseSimptoms}</td>
                                        <td>
                                            <button className="btn btn-success" onClick={() => this.deleteUser(user.iddisease)}> Delete</button>
                                            <button className="btn btn-success" onClick={() => this.editUser(user.iddisease)} style={{marginLeft: '20px'}}> Edit</button>
                                        </td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }

}

export default ListUserComponent;
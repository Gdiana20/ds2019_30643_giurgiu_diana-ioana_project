import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class EditUserComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            iddisease: '',
           diseaseDiagnosticDate: '',
           diseaseName: '',
           diseaseSimptoms: '',
                       message: null
        }
        this.saveUser = this.saveUser.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();
    }

    loadUser() {
        ApiService.fetchUserById(window.localStorage.getItem("iddisease"))
            .then((res) => {
                let user = res.data.result;
                this.setState({
                iddisease: user.iddisease,
                diseaseName: user.diseaseName,
                diseaseDiagnosticDate: user.diseaseDiagnosticDate,
                diseaseSimptoms: user.diseaseSimptoms,

                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveUser = (e) => {
        e.preventDefault();
        let user = {iddisease: this.state.iddisease, diseaseName: this.state.diseaseName, diseaseDiagnosticDate: this.state.diseaseDiagnosticDate,diseaseSimptoms: this.state.diseaseSimptoms};
        ApiService.editUser(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/diseases');
            });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit disease</h2>
                <form>

                   <div className="form-group">
                                       <label>Name:</label>
                                       <input type="text" placeholder="diseaseName" name="diseaseName" className="form-control" value={this.state.diseaseName} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Surname:</label>
                                       <input type="diseaseSurname" placeholder="diseaseSurname" name="diseaseSurname" className="form-control" value={this.state.diseaseSurname} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Email:</label>
                                       <input placeholder="Email" name="diseaseEmail" className="form-control" value={this.state.diseaseEmail} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Password:</label>
                                       <input placeholder="Password" name="diseasePassword" className="form-control" value={this.state.diseasePassword} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Gender:</label>
                                       <input placeholder="Gender" name="diseaseGender" className="form-control" value={this.state.diseaseGender} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Address:</label>
                                       <input placeholder="Address" name="diseaseAddress" className="form-control" value={this.state.diseaseAddress} onChange={this.onChange}/>
                                   </div>

                    <button className="btn btn-success" onClick={this.saveUser}>Save</button>
                </form>
            </div>
        );
    }
}

export default EditUserComponent;
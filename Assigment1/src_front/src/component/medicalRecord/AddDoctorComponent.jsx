import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class AddUserComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            idMedicalRecord: '',
            idPatient:'',
            message: null
        }
        this.saveUser = this.saveUser.bind(this);
    }

    saveUser = (e) => {
        e.preventDefault();
        let user = {MedicalRecordName: this.state.MedicalRecordName, idPatient: this.state.idPatient};
        ApiService.saveUser(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/MedicalRecords');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <h2 className="text-center">Add MedicalRecord</h2>
                <form>
                <div className="form-group">
                    <label>idMedicalRecord:</label>
                    <input type="text" placeholder="MedicalRecordName" name="MedicalRecordName" className="form-control" value={this.state.MedicalRecordName} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Surname:</label>
                    <input type="MedicalRecordSurname" placeholder="MedicalRecordSurname" name="MedicalRecordSurname" className="form-control" value={this.state.MedicalRecordSurname} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Email:</label>
                    <input placeholder="Email" name="MedicalRecordEmail" className="form-control" value={this.state.MedicalRecordEmail} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Password:</label>
                    <input placeholder="Password" name="MedicalRecordPassword" className="form-control" value={this.state.MedicalRecordPassword} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Gender:</label>
                    <input placeholder="Gender" name="MedicalRecordGender" className="form-control" value={this.state.MedicalRecordGender} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Address:</label>
                    <input placeholder="Address" name="MedicalRecordAddress" className="form-control" value={this.state.MedicalRecordAddress} onChange={this.onChange}/>
                </div>

                <button className="btn btn-success" onClick={this.saveUser}>Save</button>
            </form>
    </div>
        );
    }
}

export default AddUserComponent;
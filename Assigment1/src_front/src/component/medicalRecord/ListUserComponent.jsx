import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class ListUserComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            doctors: [],
            message: null
        }
        this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.reloadUserList = this.reloadUserList.bind(this);
    }

    componentDidMount() {
        this.reloadUserList();
    }

    reloadUserList() {
        ApiService.fetchUsers()
            .then((res) => {
                this.setState({doctors: res.data.result})
            });
    }

    deleteUser(userId) {
        ApiService.deleteUser(userId)
           .then(res => {
               this.setState({message : 'User deleted successfully.'});
               this.setState({doctors: this.state.doctors.filter(doctors => doctors.idDoctor !== userId)});
           })

    }

    editUser(id) {
        window.localStorage.setItem("idDoctor", id);
        this.props.history.push('/edit-doctor');
    }

    addUser() {
        window.localStorage.removeItem("idDoctor");
        this.props.history.push('/add-doctor');
    }

    render() {
        return (
            <div>
                <h2 className="text-center">User Details</h2>
                <button className="btn btn-danger" style={{width:'100px'}} onClick={() => this.addUser()}> Add User</button>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="hidden">Id</th>
                            <th>doctorName</th>
                            <th>doctorSurName</th>
                            <th>gender</th>
                            <th>address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.doctors.map(
                        user =>
                                    <tr key={user.idDoctor}>
                                        <td>{user.doctorName}</td>
                                        <td>{user.doctorSurname}</td>
                                        <td>{user.doctorGender}</td>
                                        <td>{user.doctorAddress}</td>
                                        <td>
                                            <button className="btn btn-success" onClick={() => this.deleteUser(user.idDoctor)}> Delete</button>
                                            <button className="btn btn-success" onClick={() => this.editUser(user.idDoctor)} style={{marginLeft: '20px'}}> Edit</button>
                                        </td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }

}

export default ListUserComponent;
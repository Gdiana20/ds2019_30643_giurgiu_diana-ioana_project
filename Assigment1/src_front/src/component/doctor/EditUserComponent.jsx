import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class EditUserComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            idDoctor: '',
            doctorName: '',
            doctorSurname: '',
                       doctorEmail: '',
                       doctorPassword: '',
                       doctorGender: '',
                       doctorAddress: '',
                       message: null
        }
        this.saveUser = this.saveUser.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();
    }

    loadUser() {
        ApiService.fetchUserById(window.localStorage.getItem("idDoctor"))
            .then((res) => {
                let user = res.data.result;
                this.setState({
                idDoctor: user.idDoctor,
                doctorName: user.doctorName,
                doctorSurname: user.doctorSurname,
                doctorEmail: user.doctorEmail,
                doctorPassword: user.doctorPassword,
                doctorGender: user.doctorGender,
                doctorAddress: user.doctorAddress,
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveUser = (e) => {
        e.preventDefault();
        let user = {idDoctor: this.state.idDoctor, doctorName: this.state.doctorName, doctorSurname: this.state.doctorSurname,doctorEmail: this.state.doctorEmail ,doctorPassword: this.state.doctorPassword, doctorGender: this.state.doctorGender, doctorAddress: this.state.doctorAddress};
        ApiService.editUser(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/doctors');
            });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit Doctor</h2>
                <form>

                   <div className="form-group">
                                       <label>Name:</label>
                                       <input type="text" placeholder="doctorName" name="doctorName" className="form-control" value={this.state.doctorName} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Surname:</label>
                                       <input type="doctorSurname" placeholder="doctorSurname" name="doctorSurname" className="form-control" value={this.state.doctorSurname} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Email:</label>
                                       <input placeholder="Email" name="doctorEmail" className="form-control" value={this.state.doctorEmail} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Password:</label>
                                       <input placeholder="Password" name="doctorPassword" className="form-control" value={this.state.doctorPassword} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Gender:</label>
                                       <input placeholder="Gender" name="doctorGender" className="form-control" value={this.state.doctorGender} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Address:</label>
                                       <input placeholder="Address" name="doctorAddress" className="form-control" value={this.state.doctorAddress} onChange={this.onChange}/>
                                   </div>

                    <button className="btn btn-success" onClick={this.saveUser}>Save</button>
                </form>
            </div>
        );
    }
}

export default EditUserComponent;
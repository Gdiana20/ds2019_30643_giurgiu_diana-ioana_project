import React, { Component } from 'react'
import PatientApiService from "../../service/PatientApiService";
import CaregiverApiService from "../../service/CaregiverApiService";
import Select from 'react-select';
import Care from "../../caregiver.model.ts"
class AddPatientComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            patientName: '',
            patientSurname: '',
            patientEmail: '',
            patientPassword: '',
            patientGender: '',
            patientAddress: '',
            Caregiver_idCaregiver: Care,
            caregivers: [],
            message: null
        }
        this.saveUser = this.saveUser.bind(this);
    }

  componentDidMount() {
        this.reloadCaregiverList();

    }
  reloadCaregiverList() {
            PatientApiService.fetchCaregivers()
                .then((res) => {
                    this.setState({caregivers: res.data.result})
                });
        }

    saveUser = (e) => {
        e.preventDefault();
        let user = {patientName: this.state.patientName, patientSurname: this.state.patientSurname, patientEmail: this.state.patientEmail, patientPassword: this.state.patientPassword, patientGender: this.state.patientGender, patientAddress: this.state.patientAddress,Caregiver_idCaregiver :this.state.Caregiver_idCaregiver};
        PatientApiService.addUser(user)
            .then(res => {
                this.setState({message : 'Patient added successfully.'});
                this.props.history.push('/patients');
            });
    }
     formatOptions=(options)=>{
            return options.map(option=>{
                return {value: option.idCaregiver , label:option.caregiverName}
            })
        }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
             console.log(this.state.caregivers)
        return(
            <div>
            <div>
                <h2 className="text-center">Add User</h2>
                <form>
                <div className="form-group">
                    <label>Name:</label>
                    <input type="text" placeholder="patientName" name="patientName" className="form-control" value={this.state.patientName} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Surname:</label>
                    <input type="patientSurname" placeholder="patientSurname" name="patientSurname" className="form-control" value={this.state.patientSurname} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Email:</label>
                    <input placeholder="Email" name="patientEmail" className="form-control" value={this.state.patientEmail} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Password:</label>
                    <input placeholder="Password" name="patientPassword" className="form-control" value={this.state.patientPassword} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Gender:</label>
                    <input placeholder="Gender" name="patientGender" className="form-control" value={this.state.patientGender} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Address:</label>
                    <input placeholder="Address" name="patientAddress" className="form-control" value={this.state.patientAddress} onChange={this.onChange}/>
                </div>
             <div className="form-group">
                 <div className="row">
                   <div className="col-md-4"></div>
                   <div className="col-md-4">
                   <label>Caregiver:</label>

<Select options={ this.formatOptions(this.state.caregivers) } name="Caregiver_idCaregiver"  onChange={(e)=>this.onChange({target:{...e , name:'Caregiver_idCaregiver'}})}/>
                   </div>
                   <div className="col-md-4"></div>
                 </div>
               </div>

                <button className="btn btn-success" onClick={this.saveUser}>Save</button>
            </form>
    </div>
     <div>
                                <h2 className="text-center">Caregiver Details</h2>
                                <table className="table table-striped">
                                    <thead>
                                        <tr>
                                            <th className="hidden">Id</th>
                                            <th>FirstName</th>
                                            <th>LastName</th>
                                            <th>caregiverEmail</th>
                                            <th>caregiverGender</th>
                                            <th>caregiverAddress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.caregivers.map(
                                        caregiver =>
                                                    <tr key={caregiver.idCaregiver}>
                                                        <td>{caregiver.caregiverName}</td>
                                                        <td>{caregiver.caregiverSurname}</td>
                                                        <td>{caregiver.caregiverEmail}</td>
                                                        <td>{caregiver.caregiverGender}</td>
                                                        <td>{caregiver.caregiverAddress}</td>
                                                    </tr>
                                            )
                                        }
                                    </tbody>
                                </table>

                            </div>
                            </div>
        );
    }
}

export default AddPatientComponent;
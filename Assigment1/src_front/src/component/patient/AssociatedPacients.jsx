import React, { Component } from 'react'
import ApiService from "../../service/PatientApiService";

class AssociatedPacient extends Component {

    constructor(props) {
        super(props)
        this.state = {
            patients: [],
            message: null
        }
        this.reloadUserList = this.reloadUserList.bind(this);

    }

    componentDidMount() {
        this.reloadUserList();
    }

    reloadUserList() {
        ApiService.seeAll()
            .then((res) => {
                this.setState({patients: res.data.result})
            });
    }


    render() {
        return (
            <div>
                <h2 className="text-center">User Details</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="hidden">Id</th>
                            <th>Name</th>
                            <th>FirstName</th>
                            <th>gender</th>
                            <th>address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.patients.map(
                        user =>
                                    <tr key={user.idPatient}>
                                        <td>{user.patientName}</td>
                                        <td>{user.patientSurname}</td>
                                        <td>{user.patientGender}</td>
                                        <td>{user.patientAddress}</td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }

}

export default AssociatedPacient;
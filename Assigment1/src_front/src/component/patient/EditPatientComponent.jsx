import React, { Component } from 'react'
import PatientApiService from "../../service/PatientApiService";

class EditPatientComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            idPatient: '',
            patientName: '',
            patientSurname: '',
                       patientEmail: '',
                       patientPassword: '',
                       patientGender: '',
                       patientAddress: '',
                       Caregiver_idCaregiver: '',
                       message: null
        }
        this.saveUser = this.saveUser.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();
    }

    loadUser() {
        PatientApiService.fetchUserById(window.localStorage.getItem("userId"))
            .then((res) => {
                let user = res.data.result;
                this.setState({
                idPatient: user.idPatient,
                patientName: user.patientName,
                patientSurname: user.patientSurname,
                patientEmail: user.patientEmail,
                patientPassword: user.patientPassword,
                patientGender: user.patientGender,
                patientAddress: user.patientAddress,
                Caregiver_idCaregiver:user.Caregiver_idCaregiver,

                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveUser = (e) => {
        e.preventDefault();
        let user = {idPatient: this.state.idPatient, patientName: this.state.patientName, patientSurname: this.state.patientSurname,patientEmail: this.state.patientEmail ,patientPassword: this.state.patientPassword, patientGender: this.state.patientGender, patientAddress: this.state.patientAddress, Caregiver_idCaregiver:this.state.Caregiver_idCaregiver,};
        PatientApiService.editUser(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/patients');
            });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit User</h2>
                <form>

                   <div className="form-group">
                                       <label>Name:</label>
                                       <input type="text" placeholder="patientName" name="patientName" className="form-control" value={this.state.patientName} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Surname:</label>
                                       <input type="patientSurname" placeholder="patientSurname" name="patientSurname" className="form-control" value={this.state.patientSurname} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Email:</label>
                                       <input placeholder="Email" name="patientEmail" className="form-control" value={this.state.patientEmail} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Password:</label>
                                       <input placeholder="Password" name="patientPassword" className="form-control" value={this.state.patientPassword} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Gender:</label>
                                       <input placeholder="Gender" name="patientGender" className="form-control" value={this.state.patientGender} onChange={this.onChange}/>
                                   </div>

                                   <div className="form-group">
                                       <label>Address:</label>
                                       <input placeholder="Address" name="patientAddress" className="form-control" value={this.state.patientAddress} onChange={this.onChange}/>
                                   </div>

                    <button className="btn btn-success" onClick={this.saveUser}>Save</button>
                </form>
            </div>
        );
    }
}

export default EditPatientComponent;
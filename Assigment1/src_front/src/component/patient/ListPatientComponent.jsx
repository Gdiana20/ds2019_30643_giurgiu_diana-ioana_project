import React, { Component } from 'react'
import PatientApiService from "../../service/PatientApiService";
class ListPatientComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            patients: [],
            message: null
        }
        this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.reloadUserList = this.reloadUserList.bind(this);
    }

    componentDidMount() {
        this.reloadUserList();

    }


    reloadUserList() {
        PatientApiService.fetchUsers()
            .then((res) => {
                this.setState({patients: res.data.result})
            });
    }

    deleteUser(userId) {
        PatientApiService.deleteUser(userId)
           .then(res => {
               this.setState({message : 'User deleted successfully.'});
               this.setState({patients: this.state.patients.filter(patients => patients.idPatient !== userId)});
           })

    }

    editUser(id) {
        window.localStorage.setItem("userId", id);
        this.props.history.push('/edit-patient');
    }

    addUser() {
        window.localStorage.removeItem("userId");
        this.props.history.push('/add-patient');
    }

    render() {
        return (

            <div>
                <h2 className="text-center">User Details</h2>
                <button className="btn btn-danger" style={{width:'100px'}} onClick={() => this.addUser()}> Add User</button>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="hidden">Id</th>
                            <th>Name</th>
                            <th>FirstName</th>
                            <th>gender</th>
                            <th>address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.patients.map(
                        user =>
                                    <tr key={user.idPatient}>
                                        <td>{user.patientName}</td>
                                        <td>{user.patientSurname}</td>
                                        <td>{user.patientGender}</td>
                                        <td>{user.patientAddress}</td>
                                        <td>
                                            <button className="btn btn-success" onClick={() => this.deleteUser(user.idPatient)}> Delete</button>
                                            <button className="btn btn-success" onClick={() => this.editUser(user.idPatient)} style={{marginLeft: '20px'}}> Edit</button>

                                        </td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>

            </div>


        );
    }

}

export default ListPatientComponent;
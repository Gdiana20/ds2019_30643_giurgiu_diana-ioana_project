import React, { Component } from 'react'
import ApiService from "../../service/MedicationApiService";

class ListMedicationComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            medications: [],
            message: null
        }
        this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.reloadUserList = this.reloadUserList.bind(this);
    }

    componentDidMount() {
        this.reloadUserList();
    }

    reloadUserList() {
        ApiService.fetchUsers()
            .then((res) => {
                this.setState({medications: res.data.result})

            });
    }

    deleteUser(userId) {
        ApiService.deleteUser(userId)
           .then(res => {
               this.setState({message : 'Medications deleted successfully.'});
               this.setState({medications: this.state.medications.filter(medications => medications.idmedication !== userId)});
           })

    }

    editUser(id) {
        window.localStorage.setItem("idmedication", id);
        this.props.history.push('/edit-medication');
    }

    addUser() {
        window.localStorage.removeItem("idmedication");
        this.props.history.push('/add-medication');
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Medication Details</h2>
                <button className="btn btn-danger" style={{width:'100px'}} onClick={() => this.addUser()}> Add Medication</button>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="hidden">Idmedication</th>
                            <th>medicationName</th>
                            <th>dosage</th>
                            <th>sideEffects</th>
                            <th>intakeIntervals</th>
                            <th>PatientID</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.medications.map(
                        user =>
                                    <tr key={user.idmedication}>
                                    <td >{user.medicationName}</td>
                                        <td>{user.dosage}</td>
                                        <td>{user.sideEffects}</td>
                                        <td>{user.intakeIntervals}</td>
                                        <td>{user.Medication_idMedication.patient.patientName}</td>
                                        <td>
                                            <button className="btn btn-success" onClick={() => this.deleteUser(user.idmedication)}> Delete</button>
                                            <button className="btn btn-success" onClick={() => this.editUser(user.idmedication)} style={{marginLeft: '20px'}}> Edit</button>
                                        </td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }

}

export default ListMedicationComponent;
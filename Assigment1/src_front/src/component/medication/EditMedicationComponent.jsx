import React, { Component } from 'react'
import ApiService from "../../service/MedicationApiService";

class EditMedicationComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
        idmedication: '',
                    medicationName: '',
                    dosage: '',
                    sideEffects: '',
                    intakeIntervals: '',
                    Medication_idMedication: '',
                       message: null
        }
        this.saveUser = this.saveUser.bind(this);
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();
    }

    loadUser() {
        ApiService.fetchUserById(window.localStorage.getItem("idmedication"))
            .then((res) => {
                let user = res.data.result;
                this.setState({
                idmedication: user.idmedication,
                medicationName: user.medicationName,
                dosage: user.dosage,
                sideEffects: user.sideEffects,
                intakeIntervals: user.intakeIntervals,
                Medication_idMedication: user.Medication_idMedication,

                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveUser = (e) => {
        e.preventDefault();
        let user = {medicationName: this.state.medicationName, dosage: this.state.dosage, sideEffects: this.state.sideEffects, intakeIntervals: this.state.intakeIntervals, Medication_idMedication: this.state.Medication_idMedication};
        ApiService.update(user)
            .then(res => {
                this.setState({message : 'medication edited successfully.'});
                this.props.history.push('/medications');
            });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit medication</h2>
                <form>
                               <div className="form-group">
                                   <label>Name:</label>
                                   <input type="text" placeholder="medicationName" name="medicationName" className="form-control" value={this.state.medicationName} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Dosage:</label>
                                   <input type="dosage" placeholder="dosage" name="dosage" className="form-control" value={this.state.dosage} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Side Effects:</label>
                                   <input placeholder="sideEffects" name="sideEffects" className="form-control" value={this.state.sideEffects} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Intake Intervals:</label>
                                   <input placeholder="intakeIntervals" name="intakeIntervals" className="form-control" value={this.state.intakeIntervals} onChange={this.onChange}/>
                               </div>

                               <div className="form-group">
                                   <label>Medication PlanID:</label>
                                   <input placeholder="Medication_idMedication" name="Medication_idMedication" className="form-control" value={this.state.Medication_idMedication} onChange={this.onChange}/>
                               </div>
                               <button className="btn btn-success" onClick={this.saveUser}>Save</button>
                           </form>
            </div>
        );
    }
}

export default EditMedicationComponent;
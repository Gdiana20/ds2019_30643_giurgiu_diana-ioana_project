import React from 'react'


import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'green',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="black" light expand="md">
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/doctors">Doctors</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                                 <NavLink href="/patients">Patients</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                                  <NavLink href="/caregivers">Caregivers</NavLink>
                          </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>
             </Nav>

            </Navbar>
    </div>
);

export default NavigationBar

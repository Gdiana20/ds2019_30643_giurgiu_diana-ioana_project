import React from 'react';
import './App.css';
import Home from './home/home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ListUserComponent from "./component/doctor/ListUserComponent";
import AddUserComponent from "./component/doctor/AddDoctorComponent";
import EditUserComponent from "./component/doctor/EditUserComponent";
import ListPatientComponent from "./component/patient/ListPatientComponent";
import AddPatientComponent from "./component/patient/AddPatientComponent";
import EditPatientComponent from "./component/patient/EditPatientComponent";
import NavigationBar from './navigation-bar'
import ListCaregiverComponent from "./component/caregiver/ListCaregiverComponent";
import AddCaregiverComponent from "./component/caregiver/AddCaregiverComponent";
import EditCaregiverComponent from "./component/caregiver/EditCaregiverComponent";
import AddRelation from "./component/caregiver/AddRelationCaregiverPatient";

import ListMedicationComponent from "./component/medication/ListMedicationComponent";
import AddMedicationComponent from "./component/medication/AddMedicationComponent";
import EditMedicationComponent from "./component/medication/EditMedicationComponent";
import AssociatedPacient from "./component/patient/ListPatientComponent";
function App() {
  return (
      <div className="container">
          <Router>
          <div>

                              <Switch>

                                       <Route path="/add-doctor" component={AddUserComponent} />

                                  <Route
                                      exact
                                      path='/'
                                      render={() => <Home/>}
                                  />

                                  <Route
                                      exact
                                      path='/doctors' component={ListUserComponent}

                                  />
                                       <Route path="/add-doctor" component={AddUserComponent} />
                                        <Route path="/edit-doctor" component={EditUserComponent} />
                                       <Route path="/patients" component={ListPatientComponent} />
                                     <Route path="/edit-patient" component={EditPatientComponent} />
                                     <Route path="/add-patient" component={AddPatientComponent} />
                                     <Route path="/add-caregiver" component={AddCaregiverComponent} />
                                     <Route path="/edit-caregiver" component={EditCaregiverComponent} />
                                     <Route path="/caregivers" component={ListCaregiverComponent} />
                                     <Route path="/medications" component={ListMedicationComponent} />
                                     <Route path="/add-medication" component={AddMedicationComponent} />
                                     <Route path="/edit-medication" component={EditMedicationComponent} />
                                                                </Switch>
                          </div>
          </Router>
      </div>
  );
}
export default App;

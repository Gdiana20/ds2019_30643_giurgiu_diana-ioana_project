package com.javaspringclub;

import com.javaspringclub.Model.FileReading;
import com.javaspringclub.Model.PacientSensor;
import com.javaspringclub.Model.Rules;
import movies.wsdl.*;
//import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.*;

import java.awt.*;

import java.awt.event.*;
import java.util.List;

import javax.swing.*;

import javax.swing.table.TableModel;

import javax.swing.table.DefaultTableModel;

import javax.swing.table.*;

public class newframe extends JFrame {

    private JPanel topPanel, topPanel1;
    private JTable table;
    private JScrollPane scrollPane, scrollPane1;
    private String[] columnNames = new String[4];
    private String[][] dataValues = new String[3][4];
    JButton button = new JButton("History");
    String rec;

    public newframe(GetAllPatientsResponse pat, GetAllMedicationsResponse meds) {
        setTitle("Patients");
        setSize(300, 300);
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);
        columnNames = new String[]{"Name", "Surname", "Email", "Button"};

       for(int i=0;i< pat.getPatientType().size(); i++)
       {
           dataValues[i] = new String[]{pat.getPatientType().get(i).getPatientName(),pat.getPatientType().get(i).getPatientSurname(),pat.getPatientType().get(i).getPatientEmail()};
       }
       // dataValues = new String[][]{{pat.getPatientType().getPatientName(), pat.getPatientType().getPatientSurname(), pat.getPatientType().getPatientEmail()}};
        TableModel model = new myTableModel("pacientTable");
        table = new JTable();
        table.setModel(model);
        table.getColumn("Button").setCellRenderer(new ButtonRenderer());
        table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
        scrollPane = new JScrollPane(table);
        topPanel.add(scrollPane, BorderLayout.CENTER);
        FileReading fl = new FileReading();
        List<String> listaAct = fl.listaDespartita;
        int count=0;
        for(String str: listaAct)
        {
            String start = fl.dataMaker(listaAct.get(count), listaAct.get(count + 1));
            String end = fl.dataMaker(listaAct.get(count + 2), listaAct.get(count + 3));
            String act = listaAct.get(count + 4);
            //System.out.println(start + end + act);
            PacientSensor mes = new PacientSensor("1", start, end, act);
            Rules applyRules= new Rules(mes);
            count=count+1;
        }
        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        MedicationList med= new MedicationList(meds);
                        PieChart_AWT demo = new PieChart_AWT( "Pacient Activity " );
                        demo.setSize( 560 , 367 );
                        med.setSize(550,550);
                       // RefineryUtilities.centerFrameOnScreen( demo );
                        demo.setVisible( true );
                        med.setVisible(true);
                        //JOptionPane.showMessageDialog(null, "Button Clicked in JTable Cell");
                        rec=med.sendRec();
                    }
                }
        );
    }

    class ButtonRenderer extends JButton implements TableCellRenderer {
        public ButtonRenderer() {
            setOpaque(true);
        }
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }

    class ButtonEditor extends DefaultCellEditor {

        private String label;

        public ButtonEditor(JCheckBox checkBox) {

            super(checkBox);

        }

        public Component getTableCellEditorComponent(JTable table, Object value,

                                                     boolean isSelected, int row, int column) {

            label = (value == null) ? "" : value.toString();

            button.setText(label);

            return button;

        }

        public Object getCellEditorValue() {

            return new String(label);

        }

    }

    public class myTableModel extends DefaultTableModel {

        String dat;

        JButton button = new JButton("History");

        myTableModel(String tname) {

            super(dataValues, columnNames);

            dat = tname;

        }

        public boolean isCellEditable(int row, int cols) {

            if (dat == "pacientTable") {

                if (cols == 0) {
                    return false;
                }

            }

            return true;

        }

    }

    public String sendRec()
    {
        return rec;
    }

}
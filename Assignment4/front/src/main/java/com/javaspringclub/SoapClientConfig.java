package com.javaspringclub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapClientConfig {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("doctor.wsdl");
		return marshaller;
	}

	@Bean
	public DoctorClient dctClient(Jaxb2Marshaller marshaller) {
		DoctorClient client = new DoctorClient();
		client.setDefaultUri("http://localhost:8080/ws/meds");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}
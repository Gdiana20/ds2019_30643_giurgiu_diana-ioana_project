package com.javaspringclub;

import movies.wsdl.GetAllMedicationsResponse;
import movies.wsdl.GetAllPatientsResponse;
import movies.wsdl.GetPatientByIdResponse;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CaregiverPane extends JFrame  {

    private JPanel topPanel, topPanel1;
    private JTable table;
    private JScrollPane scrollPane, scrollPane1;
    private String[] columnNames = new String[4];
    private String[][] dataValues = new String[3][4];
   // JButton button = new JButton();

    public CaregiverPane(GetAllPatientsResponse pat) {
        setTitle("Button in JTable Cell");
        setSize(300, 300);
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);
        columnNames = new String[]{"Name", "Surname", "Email", "Recomandation"};
//       if(rec.sendRec().equalsIgnoreCase(""))
       // dataValues = new String[][]{{pat.getPatientType().getPatientName(), pat.getPatientType().getPatientSurname(), pat.getPatientType().getPatientEmail(),"more meat"}};
        for(int i=0;i< pat.getPatientType().size(); i++)
        {
            dataValues[i] = new String[]{pat.getPatientType().get(i).getPatientName(),pat.getPatientType().get(i).getPatientSurname(),pat.getPatientType().get(i).getPatientEmail()};
        }

        TableModel model = new myTableModel("pacientTable");
        table = new JTable();
        table.setModel(model);
    //    table.getColumn("Button").setCellRenderer(new ButtonRenderer());
     //   table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
        scrollPane = new JScrollPane(table);
        topPanel.add(scrollPane, BorderLayout.CENTER);
      //  table.getColumn("Recomandation").;
    }

    public class myTableModel extends DefaultTableModel {

        String dat;

        JButton button = new JButton("");

        myTableModel(String tname) {

            super(dataValues, columnNames);

            dat = tname;

        }

        public boolean isCellEditable(int row, int cols) {

            if (dat == "pacientTable") {

                if (cols == 0) {
                    return false;
                }

            }

            return true;

        }

    }




}

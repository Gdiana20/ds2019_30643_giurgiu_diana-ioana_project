package com.javaspringclub;

import com.javaspringclub.Model.FileReading;
import com.javaspringclub.Model.PacientSensor;
import com.javaspringclub.Model.Rules;
import movies.wsdl.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.*;
import java.util.List;

public class RunClient {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SoapClientConfig.class);
        MovieClient client = context.getBean(MovieClient.class);
       // DoctorClient cli= context.getBean(DoctorClient.class);
        GetDoctorByIdResponse resp= client.getDoctorById(2);
        GetMovieByIdResponse response = client.getMovieById(new Long(102));
        GetPatientByIdResponse pat= client.getPatientById(2);
        GetCaregiverByIdResponse care=client.getCaregiverById(1);
        GetMedicationByIdResponse med= client.getMedicationById(1);
        GetAllMedicationsResponse allMeds= client.getAllMedication();
        GetAllPatientsResponse allpats= client.getAllPatient();

        System.out.println("response: Movie id="+ response.getMovieType().getMovieId()+", title=" + response.getMovieType().getTitle() + ", category="+ response.getMovieType().getCategory());
        System.out.println("response: Movie id="+ resp.getDoctorType().getIdDoctor()+ resp.getDoctorType().getDoctorName());
        System.out.println(med.getMedicationType().getIdmedication()+ "   "+med.getMedicationType().getMedicationName());
        System.out.println(care.getCaregiverType().getCaregiverName());
        Log frameTabel = new Log(resp.getDoctorType().getDoctorName(),resp.getDoctorType().getDoctorPassword(),allpats,allMeds,care);

       //allMeds.getMedicationType().toArray();
        System.out.println(allMeds.getMedicationType().get(0).getMedicationName());
        System.out.println(allMeds.getMedicationType().get(1).getMedicationName());
        System.out.println(allMeds.getMedicationType().get(2).getMedicationName());







    }

}
package com.javaspringclub;

import movies.wsdl.GetDoctorByIdRequest;
import movies.wsdl.GetDoctorByIdResponse;
import movies.wsdl.GetMovieByIdRequest;
import movies.wsdl.GetMovieByIdResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class DoctorClient  extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(DoctorClient.class);

    public GetDoctorByIdResponse getDoctorById(int id) {
        GetDoctorByIdRequest request = new GetDoctorByIdRequest();
        request.setIdDoctor(id);

        log.info("Requesting Doctor By id= " + id);
        return (GetDoctorByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);

    }
	
	public GetDoctorByIdResponse getDoctorById(int id) {
		GetDoctorByIdRequest request = new GetDoctorByIdRequest();
		request.setIdDoctor(id);

		log.info("Requesting Doctor By id= " + id);
		return (GetDoctorByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);

	}
	public GetPatientByIdResponse getPatientById(int id) {
		GetPatientByIdRequest request = new GetPatientByIdRequest();
		request.setIdPatient(id);

		log.info("Requesting Patient By id= " + id);
		return (GetPatientByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);

	}
	public GetCaregiverByIdResponse getCaregiverById(int id) {
		GetCaregiverByIdRequest request = new GetCaregiverByIdRequest();
		request.setIdCaregiver(id);

		log.info("Requesting Caregiver By id= " + id);
		return (GetCaregiverByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);

	}
	public GetMedicationByIdResponse getMedicationById(int id) {
		GetMedicationByIdRequest request = new GetMedicationByIdRequest();
		request.setIdmedication(id);

		log.info("Requesting Medication By id= " + id);
		return (GetMedicationByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);

	}

	public GetAllMedicationsResponse getAllMedication() {
		GetAllMedicationsRequest request = new GetAllMedicationsRequest();
		//request.setIdmedication(id);

		//log.info("Requesting Medication By id= " + id);
		return (GetAllMedicationsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public GetAllPatientsResponse getAllPatient() {
		GetAllPatientsRequest request = new GetAllPatientsRequest();
		//request.setIdmedication(id);

		//log.info("Requesting Medication By id= " + id);
		return (GetAllPatientsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}
}

package com.javaspringclub.Model;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileReading {

    private static BufferedReader br = null;
    private List<String> list;
    public List<String> listaDespartita;
    ArrayList<String> oneLine = new ArrayList<String>();

    static {
        try {
            br = new BufferedReader(new FileReader("activity.txt"), 10);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public FileReading() {
        readFileLineByLine();
    }

    public void readFileLineByLine() {
        String line = null;
        int count = 0;
        while (true) {
            this.list = new ArrayList<String>();
            this.listaDespartita = new ArrayList<String>();
            synchronized (br) {
                try {
                    while ((line = br.readLine()) != null) {
                        list.add(line);
                        oneLine.clear();
                        String[] splited = line.split("\\s+");
                        for (String split : splited) {
                            listaDespartita.add(split);
                            oneLine.add(split);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (line == null)
                System.out.println("now you stop");
            break;
        }
    }

    public String dataMaker(String data, String ora) {
        String dataMade = "";
        String[] datele = data.split("-");
        String[] orele = ora.split(":");
        for (String st : datele) {
            dataMade = dataMade + st;
        }

        for (String st : orele) {
            dataMade = dataMade + st;
        }
        return dataMade;
    }

    public void display(List<String> list) {
        for (String str : listaDespartita) {
            System.out.println(str);
        }
        System.out.println(list.size());
    }

    public List<String> getListaDespartita() {
        return listaDespartita;
    }
}

package com.javaspringclub.Model;


import java.io.Serializable;

public class PacientSensor implements Serializable {
    private final String id;
    private final String start;
    private final String end;
    private final String activity;

    public PacientSensor( String id,  String start,  String end, String activity) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.activity = activity;
    }

    public String getId() {
        return id;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getActivity() {
        return activity;
    }

    @Override
    public String toString() {
        return "PacientSensor{" +
                "id='" + id + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", activity='" + activity + '\'' +
                '}';
    }
}

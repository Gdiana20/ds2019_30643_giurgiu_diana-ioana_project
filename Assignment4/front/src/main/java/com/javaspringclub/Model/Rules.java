package com.javaspringclub.Model;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Rules {
    /*
    * R1: The sleep period longer than 12 hours;
     R2: The leaving activity (outdoor) is longer than 12 hours;
     R3: The period spent in bathroom is longer than 1 hour;
    * */
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    boolean problem=false;
    public Rules(PacientSensor activity) {

        if (RuleOne(activity) & RuleThree(activity) & RuleTwo(activity)) {
            System.out.println("No problem here");
        } else {
            ////alarma notificare push
            System.out.println("THE PACIENT HAS A PROBLEM!!!");
            JOptionPane.showMessageDialog(null, "THE PACIENT HAS A PROBLEM!!!");
            problem=true;
        }

    }

    public boolean RuleOne(PacientSensor activity) {
        if (activity.getActivity().equalsIgnoreCase("Sleeping")) {
            Date start = createDate(activity.getStart());
            Date end = createDate(activity.getEnd());
            long diff = end.getTime() - start.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;
            System.out.println(diffHours);
            if (diffHours >= 12) {
                System.out.println("The patient slept for longer or close to 12h ");
                JOptionPane.showMessageDialog(null, "The patient slept for longer or close to 12h");
                return false;
            } else return true;
        } else return true;
    }

    public boolean RuleTwo(PacientSensor activity) {
        if (activity.getActivity().equalsIgnoreCase("Leaving")) {
            Date start = createDate(activity.getStart());
            Date end = createDate(activity.getEnd());
            long diff = end.getTime() - start.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;
            if (diffHours >= 12) {
                System.out.println("The patient left for longer or close to 12h ");
                JOptionPane.showMessageDialog(null, "The patient left for longer or close to 12h");
                return false;
            } else return true;
        } else return true;
    }

    public boolean RuleThree(PacientSensor activity) {
        if (activity.getActivity().equalsIgnoreCase("Toileting") || activity.getActivity().equalsIgnoreCase("Showering")) {
            Date start = createDate(activity.getStart());
            Date end = createDate(activity.getEnd());
            long diff = end.getTime() - start.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;
            if (diffHours >= 1) {
                System.out.println("The patient was in the bathroom for longer than I hour ");
                JOptionPane.showMessageDialog(null, "The patient was in the bathroom for longer than I hour");

                return false;
            } else return true;
        } else return true;
    }

    public Date createDate(String data) {
        String ora = data.substring(6, 11);
        String ziua = data.substring(0, 5);
        try {
            Date date1 = new SimpleDateFormat("yyyyMMddHHmmss").parse(data);
            return date1;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }


}

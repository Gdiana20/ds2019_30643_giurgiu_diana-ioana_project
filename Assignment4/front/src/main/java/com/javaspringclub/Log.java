package com.javaspringclub;

import movies.wsdl.GetAllMedicationsResponse;
import movies.wsdl.GetAllPatientsResponse;
import movies.wsdl.GetCaregiverByIdResponse;
import movies.wsdl.GetPatientByIdResponse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Log extends JFrame {


    JButton blogin = new JButton("Login");
    JPanel panel = new JPanel();
    JTextField txuser = new JTextField(15);
    JPasswordField pass = new JPasswordField(15);
    String reco;

    Log(String doctorName, String doctorPassword, GetAllPatientsResponse pat, GetAllMedicationsResponse meds, GetCaregiverByIdResponse caregiver) {
        super("Login Autentification");
        setSize(300, 200);
        setLocation(500, 280);
        panel.setLayout(null);


        txuser.setBounds(70, 30, 150, 20);
        pass.setBounds(70, 65, 150, 20);
        blogin.setBounds(110, 100, 80, 20);

        panel.add(blogin);
        panel.add(txuser);
        panel.add(pass);

        getContentPane().add(panel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        actionlogin(doctorName, doctorPassword, pat, meds,caregiver);
    }

    public void actionlogin(String doctorName, String doctorPassword, GetAllPatientsResponse pat, GetAllMedicationsResponse meds,GetCaregiverByIdResponse caregiver) {
        blogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                String puname = txuser.getText();
                String ppaswd = pass.getText();
                if (puname.equals(doctorName) && ppaswd.equals(doctorPassword)) {
                    newframe regFace = new newframe(pat, meds);
                    regFace.setVisible(true);
                    dispose();
                    reco=regFace.sendRec();
                } else if (puname.equals(caregiver.getCaregiverType().getCaregiverName()) && ppaswd.equals(caregiver.getCaregiverType().getCaregiverPassword())) {
                    CaregiverPane regFace = new CaregiverPane(pat);
                    regFace.setVisible(true);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Wrong Password / Username");
                    txuser.setText("");
                    pass.setText("");
                    txuser.requestFocus();
                }

            }
        });
    }
}

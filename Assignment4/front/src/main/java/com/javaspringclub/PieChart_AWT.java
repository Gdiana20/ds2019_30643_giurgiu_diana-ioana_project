package com.javaspringclub;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.swing.*;

public class PieChart_AWT extends ApplicationFrame {

    public PieChart_AWT(String title) {
        super(title);
        setContentPane(createDemoPanel());
    }

    private static PieDataset createDataset() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("sleeping", new Double(14));
        dataset.setValue("showering", new Double(14));
        dataset.setValue("breakfast", new Double(14));
        dataset.setValue("grooming", new Double(51));
        dataset.setValue("Spare_Time/TV", new Double(77));
        dataset.setValue("toileting", new Double(44));
        dataset.setValue("snack", new Double(11));
        dataset.setValue("lunch", new Double(11));
        dataset.setValue("leaving", new Double(14));
        return dataset;
    }

    private static JFreeChart createChart(PieDataset dataset) {
        JFreeChart chart = ChartFactory.createPieChart(
                "Activity chart",   // chart title
                dataset,          // data
                true,             // include legend
                true,
                false);

        return chart;
    }

    public static JPanel createDemoPanel() {
        JFreeChart chart = createChart(createDataset());
        return new ChartPanel(chart);
    }
}
package com.javaspringclub;

import movies.wsdl.GetAllMedicationsResponse;
import org.apache.logging.log4j.message.StringFormattedMessage;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MedicationList extends JFrame {
    private JPanel topPanel, topPanel1;
    private JTable table;
    private JScrollPane scrollPane, scrollPane1;
    private String[] columnNames = new String[5];
    private String[][] dataValues = new String[3][5];
    JButton button = new JButton();
    String sendRec;

    public MedicationList(GetAllMedicationsResponse allMeds) {
        setTitle("Button in JTable Cell");
        setSize(300, 300);
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);
        columnNames = new String[]{"Name", "Dosage", "Intake Intervals", "Side Effects", "Button"};

        for (int i = 0; i < allMeds.getMedicationType().size(); i++) {
            dataValues[i] = new String[]{allMeds.getMedicationType().get(i).getMedicationName(),
                    allMeds.getMedicationType().get(i).getDosage(),
                    String.valueOf(allMeds.getMedicationType().get(i).getIntakeIntervals()),
                    allMeds.getMedicationType().get(i).getSideEffects()};
        }
        // dataValues = new String[][]{{}};
        TableModel model = new myTableModel("MedicationTable");
        table = new JTable();
        table.setModel(model);
        table.getColumn("Button").setCellRenderer(new ButtonRenderer());
        table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
        scrollPane = new JScrollPane(table);
        topPanel.add(scrollPane, BorderLayout.CENTER);
        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        Recomandation rec= new Recomandation(allMeds);
                        sendRec= rec.sendRec();
                    }
                }
        );
    }

    class ButtonRenderer extends JButton implements TableCellRenderer {
        public ButtonRenderer() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }

    public String sendRec()
    {
        return sendRec;
    }
    class ButtonEditor extends DefaultCellEditor {

        private String label;

        public ButtonEditor(JCheckBox checkBox) {

            super(checkBox);

        }

        public Component getTableCellEditorComponent(JTable table, Object value,

                                                     boolean isSelected, int row, int column) {

            label = (value == null) ? "" : value.toString();

            button.setText(label);

            return button;

        }

        public Object getCellEditorValue() {

            return new String(label);

        }

    }

    public class myTableModel extends DefaultTableModel {

        String dat;

        JButton button = new JButton("");

        myTableModel(String tname) {

            super(dataValues, columnNames);

            dat = tname;

        }

        public boolean isCellEditable(int row, int cols) {

            if (dat == "MedicationTable") {

                if (cols == 0) {
                    return false;
                }

            }

            return true;

        }

    }


}




package com.javaspringclub.config;

import com.javaspringclub.endpoint.*;
import com.javaspringclub.entity.Doctor;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext appContext){
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(appContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

  

//    // localhost:8080/ws/movies.wsdl
    @Bean(name = "doctor")
    public DefaultWsdl11Definition defaultWsdlDoctorDefinition(XsdSchema schema){
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MedPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace(DoctorEndpoint.NAMESPACE_URI);
        wsdl11Definition.setSchema(schema);
        return wsdl11Definition;
    }

    @Bean(name = "patient")
    public DefaultWsdl11Definition defaultWsdlPatientDefinition(XsdSchema schema){
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MedPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace(PatientEndpoint.NAMESPACE_URI);
        wsdl11Definition.setSchema(schema);
        return wsdl11Definition;
    }
    @Bean(name = "caregiver")
    public DefaultWsdl11Definition defaultWsdlCaregiverDefinition(XsdSchema schema){
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MedPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace(CaregiverEndpoint.NAMESPACE_URI);
        wsdl11Definition.setSchema(schema);
        return wsdl11Definition;
    }


    @Bean(name = "medication")
    public DefaultWsdl11Definition defaultWsdlMedicationDefinition(XsdSchema schema){
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MedPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace(MedicationEndpoint.NAMESPACE_URI);
        wsdl11Definition.setSchema(schema);
        return wsdl11Definition;
    }
    @Bean
    public XsdSchema doctorSchema(){
        return new SimpleXsdSchema(new ClassPathResource("/xsd/doctor.xsd"));
    }
	 @Bean
    public XsdSchema caregiverSchema(){
        return new SimpleXsdSchema(new ClassPathResource("/xsd/caregiver.xsd"));
    }
	 @Bean
    public XsdSchema patientSchema(){
        return new SimpleXsdSchema(new ClassPathResource("/xsd/patient.xsd"));
    }
	 @Bean
    public XsdSchema medicationSchema(){
        return new SimpleXsdSchema(new ClassPathResource("/xsd/medication.xsd"));
    }

}

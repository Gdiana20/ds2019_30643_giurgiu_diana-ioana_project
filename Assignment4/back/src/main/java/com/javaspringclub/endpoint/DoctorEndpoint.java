package com.javaspringclub.endpoint;

import com.javaspringclub.entity.Doctor;
import com.javaspringclub.entity.MovieEntity;
import com.javaspringclub.gs_ws.*;
import com.javaspringclub.service.DoctorService;
import com.javaspringclub.service.MovieEntityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class DoctorEndpoint {

    public static final String NAMESPACE_URI = "http://www.javaspringclub.com/movies-ws";

    private DoctorService service;

    public DoctorEndpoint() {

    }

    @Autowired
    public DoctorEndpoint(DoctorService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getDoctorByIdRequest")
    @ResponsePayload
    public GetDoctorByIdResponse getDoctorById(@RequestPayload GetDoctorByIdRequest request) {
        GetDoctorByIdResponse response = new GetDoctorByIdResponse();
        Doctor movieEntity = service.getEntityById(request.getIdDoctor());
        DoctorType movieType = new DoctorType();
        BeanUtils.copyProperties(movieEntity, movieType);
        response.setDoctorType(movieType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllMDoctorsRequest")
    @ResponsePayload
    public GetAllDoctorsResponse getAllDoctors(@RequestPayload GetAllDoctorsRequest request) {
        GetAllDoctorsResponse response = new GetAllDoctorsResponse();
        List<DoctorType> movieTypeList = new ArrayList<DoctorType>();
        List<Doctor> movieEntityList = service.getAllEntities();
        for (Doctor entity : movieEntityList) {
            DoctorType movieType = new DoctorType();
            BeanUtils.copyProperties(entity, movieType);
            movieTypeList.add(movieType);
        }
        response.getDoctorType().addAll(movieTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addDoctorRequest")
    @ResponsePayload
    public AddDoctorResponse addDoctor(@RequestPayload AddDoctorRequest request) {
        AddDoctorResponse response = new AddDoctorResponse();
        DoctorType newMovieType = new DoctorType();
        ServiceStatus serviceStatus = new ServiceStatus();

        Doctor newMovieEntity = new Doctor(request.getDoctorName(), request.getDoctorEmail(),request.getDoctorPassword(),request.getDoctorSurname());
        Doctor savedMovieEntity = service.addEntity(newMovieEntity);

        if (savedMovieEntity == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Entity");
        } else {

            BeanUtils.copyProperties(savedMovieEntity, newMovieType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setDoctorType(newMovieType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateDoctorRequest")
    @ResponsePayload
    public UpdateDoctorResponse updateDoctor(@RequestPayload UpdateDoctorRequest request) {
        UpdateDoctorResponse response = new UpdateDoctorResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        // 1. Find if movie available
        Doctor movieFromDB = service.getEntityByTitle(request.getDoctorName());

        if(movieFromDB == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Movie = " + request.getDoctorName() + " not found");
        }else {

            // 2. Get updated movie information from the request
            movieFromDB.setDoctorName(request.getDoctorName());
            movieFromDB.setDoctorEmail(request.getDoctorEmail());
            movieFromDB.setDoctorPassword(request.getDoctorPassword());
            movieFromDB.setDoctorSurname(request.getDoctorSurname());
            // 3. update the movie in database

            boolean flag = service.updateEntity(movieFromDB);

            if(flag == false) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getDoctorName());;
            }else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }


        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteDoctorRequest")
    @ResponsePayload
    public DeleteDoctorResponse deleteDoctor(@RequestPayload DeleteDoctorRequest request) {
        DeleteDoctorResponse response = new DeleteDoctorResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        boolean flag = service.deleteEntityById(request.getIdDoctor());

        if (flag == false) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getIdDoctor());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }


}

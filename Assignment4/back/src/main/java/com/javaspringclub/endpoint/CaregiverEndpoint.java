package com.javaspringclub.endpoint;


import com.javaspringclub.entity.Caregiver;
import com.javaspringclub.gs_ws.*;
import com.javaspringclub.service.CaregiverService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class CaregiverEndpoint {

    public static final String NAMESPACE_URI = "http://www.javaspringclub.com/movies-ws";

    private CaregiverService service;

    public CaregiverEndpoint() {

    }

    @Autowired
    public CaregiverEndpoint(CaregiverService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCaregiverByIdRequest")
    @ResponsePayload
    public GetCaregiverByIdResponse getCaregiverById(@RequestPayload GetCaregiverByIdRequest request) {
        GetCaregiverByIdResponse response = new GetCaregiverByIdResponse();
        Caregiver movieEntity = service.getEntityById(request.getIdCaregiver());
        CaregiverType movieType = new CaregiverType();
        BeanUtils.copyProperties(movieEntity, movieType);
        response.setCaregiverType(movieType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllMCaregiversRequest")
    @ResponsePayload
    public GetAllCaregiversResponse getAllCaregivers(@RequestPayload GetAllCaregiversRequest request) {
        GetAllCaregiversResponse response = new GetAllCaregiversResponse();
        List<CaregiverType> movieTypeList = new ArrayList<CaregiverType>();
        List<Caregiver> movieEntityList = service.getAllEntities();
        for (Caregiver entity : movieEntityList) {
            CaregiverType movieType = new CaregiverType();
            BeanUtils.copyProperties(entity, movieType);
            movieTypeList.add(movieType);
        }
        response.getCaregiverType().addAll(movieTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addCaregiverRequest")
    @ResponsePayload
    public AddCaregiverResponse addCaregiver(@RequestPayload AddCaregiverRequest request) {
        AddCaregiverResponse response = new AddCaregiverResponse();
        CaregiverType newMovieType = new CaregiverType();
        ServiceStatus serviceStatus = new ServiceStatus();

        Caregiver newMovieEntity = new Caregiver(request.getCaregiverName(), request.getCaregiverEmail(), request.getCaregiverPassword(), request.getCaregiverSurname());
        Caregiver savedMovieEntity = service.addEntity(newMovieEntity);

        if (savedMovieEntity == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Entity");
        } else {

            BeanUtils.copyProperties(savedMovieEntity, newMovieType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setCaregiverType(newMovieType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateCaregiverRequest")
    @ResponsePayload
    public UpdateCaregiverResponse updateCaregiver(@RequestPayload UpdateCaregiverRequest request) {
        UpdateCaregiverResponse response = new UpdateCaregiverResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        // 1. Find if movie available
        Caregiver movieFromDB = service.getEntityByTitle(request.getCaregiverName());

        if (movieFromDB == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Movie = " + request.getCaregiverName() + " not found");
        } else {

            // 2. Get updated movie information from the request
            movieFromDB.setCaregiverName(request.getCaregiverName());
            movieFromDB.setCaregiverEmail(request.getCaregiverEmail());
            movieFromDB.setCaregiverPassword(request.getCaregiverPassword());
            movieFromDB.setCaregiverSurname(request.getCaregiverSurname());
            // 3. update the movie in database

            boolean flag = service.updateEntity(movieFromDB);

            if (flag == false) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getCaregiverName());
                ;
            } else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }


        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteCaregiverRequest")
    @ResponsePayload
    public DeleteCaregiverResponse deleteCaregiver(@RequestPayload DeleteCaregiverRequest request) {
        DeleteCaregiverResponse response = new DeleteCaregiverResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        boolean flag = service.deleteEntityById(request.getIdCaregiver());

        if (flag == false) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getIdCaregiver());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }
}

package com.javaspringclub.endpoint;


import com.javaspringclub.entity.Medication;
import com.javaspringclub.gs_ws.*;
import com.javaspringclub.service.MedicationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class MedicationEndpoint {

    public static final String NAMESPACE_URI = "http://www.javaspringclub.com/movies-ws";

    private MedicationService service;

    public MedicationEndpoint() {

    }

    @Autowired
    public MedicationEndpoint(MedicationService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMedicationByIdRequest")
    @ResponsePayload
    public GetMedicationByIdResponse getMedicationById(@RequestPayload GetMedicationByIdRequest request) {
        GetMedicationByIdResponse response = new GetMedicationByIdResponse();
        Medication movieEntity = service.getEntityById(request.getIdmedication());
        MedicationType movieType = new MedicationType();
        BeanUtils.copyProperties(movieEntity, movieType);
        response.setMedicationType(movieType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllMedicationsRequest")
    @ResponsePayload
    public GetAllMedicationsResponse getAllMedications(@RequestPayload GetAllMedicationsRequest request) {
        GetAllMedicationsResponse response = new GetAllMedicationsResponse();
        List<MedicationType> movieTypeList = new ArrayList<MedicationType>();
        List<Medication> movieEntityList = service.getAllEntities();
        for (Medication entity : movieEntityList) {
            MedicationType movieType = new MedicationType();
            BeanUtils.copyProperties(entity, movieType);
            movieTypeList.add(movieType);
        }
        response.getMedicationType().addAll(movieTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addMedicationRequest")
    @ResponsePayload
    public AddMedicationResponse addMedication(@RequestPayload AddMedicationRequest request) {
        AddMedicationResponse response = new AddMedicationResponse();
        MedicationType newMovieType = new MedicationType();
        ServiceStatus serviceStatus = new ServiceStatus();

        Medication newMovieEntity = new Medication(request.getMedicationName(), request.getSideEffects(), request.getDosage(), request.getIntakeIntervals());
        Medication savedMovieEntity = service.addEntity(newMovieEntity);

        if (savedMovieEntity == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Entity");
        } else {

            BeanUtils.copyProperties(savedMovieEntity, newMovieType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setMedicationType(newMovieType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

//    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateMedicationRequest")
//    @ResponsePayload
//    public UpdateMedicationResponse updateMedication(@RequestPayload UpdateMedicationPlanRequest request) {
//        UpdateMedicationPlanResponse response = new UpdateMedicationPlanResponse();
//        ServiceStatus serviceStatus = new ServiceStatus();
//        // 1. Find if movie available
//        MedicationPlan movieFromDB = service.getEntityById(request.gett());
//
//        if (movieFromDB == null) {
//            serviceStatus.setStatusCode("NOT FOUND");
//            serviceStatus.setMessage("Movie = " + request.getMedicationPlanName() + " not found");
//        } else {
//
//            // 2. Get updated movie information from the request
//            movieFromDB.setMedicationPlanName(request.getMedicationPlanName());
//            movieFromDB.setMedicationPlanEmail(request.getMedicationPlanEmail());
//            movieFromDB.setMedicationPlanPassword(request.getMedicationPlanPassword());
//            movieFromDB.setMedicationPlanSurname(request.getMedicationPlanSurname());
//            // 3. update the movie in database
//
//            boolean flag = service.updateEntity(movieFromDB);
//
//            if (flag == false) {
//                serviceStatus.setStatusCode("CONFLICT");
//                serviceStatus.setMessage("Exception while updating Entity=" + request.getMedicationPlanName());
//                ;
//            } else {
//                serviceStatus.setStatusCode("SUCCESS");
//                serviceStatus.setMessage("Content updated Successfully");
//            }
//
//
//        }
//
//        response.setServiceStatus(serviceStatus);
//        return response;
//    }
//
//    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteMedicationPlanRequest")
//    @ResponsePayload
//    public DeleteMedicationPlanResponse deleteMedicationPlan(@RequestPayload DeleteMedicationPlanRequest request) {
//        DeleteMedicationPlanResponse response = new DeleteMedicationPlanResponse();
//        ServiceStatus serviceStatus = new ServiceStatus();
//
//        boolean flag = service.deleteEntityById(request.getIdMedicationPlan());
//
//        if (flag == false) {
//            serviceStatus.setStatusCode("FAIL");
//            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getIdMedicationPlan());
//        } else {
//            serviceStatus.setStatusCode("SUCCESS");
//            serviceStatus.setMessage("Content Deleted Successfully");
//        }
//
//        response.setServiceStatus(serviceStatus);
//        return response;
//    }
}

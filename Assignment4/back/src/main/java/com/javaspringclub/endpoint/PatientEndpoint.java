package com.javaspringclub.endpoint;


import com.javaspringclub.entity.Patient;
import com.javaspringclub.gs_ws.*;
import com.javaspringclub.service.PatientService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class PatientEndpoint {

    public static final String NAMESPACE_URI = "http://www.javaspringclub.com/movies-ws";

    private PatientService service;

    public PatientEndpoint() {

    }

    @Autowired
    public PatientEndpoint(PatientService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPatientByIdRequest")
    @ResponsePayload
    public GetPatientByIdResponse getPatientById(@RequestPayload GetPatientByIdRequest request) {
        GetPatientByIdResponse response = new GetPatientByIdResponse();
        Patient movieEntity = service.getEntityById(request.getIdPatient());
        PatientType movieType = new PatientType();
        BeanUtils.copyProperties(movieEntity, movieType);
        response.setPatientType(movieType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPatientsRequest")
    @ResponsePayload
    public GetAllPatientsResponse getAllPatients(@RequestPayload GetAllPatientsRequest request) {
        GetAllPatientsResponse response = new GetAllPatientsResponse();
        List<PatientType> movieTypeList = new ArrayList<PatientType>();
        List<Patient> movieEntityList = service.getAllEntities();
        for (Patient entity : movieEntityList) {
            PatientType movieType = new PatientType();
            BeanUtils.copyProperties(entity, movieType);
            movieTypeList.add(movieType);
        }
        response.getPatientType().addAll(movieTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPatientRequest")
    @ResponsePayload
    public AddPatientResponse addPatient(@RequestPayload AddPatientRequest request) {
        AddPatientResponse response = new AddPatientResponse();
        PatientType newMovieType = new PatientType();
        ServiceStatus serviceStatus = new ServiceStatus();

        Patient newMovieEntity = new Patient(request.getPatientName(), request.getPatientEmail(), request.getPatientPassword(), request.getPatientSurname());
        Patient savedMovieEntity = service.addEntity(newMovieEntity);

        if (savedMovieEntity == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Entity");
        } else {

            BeanUtils.copyProperties(savedMovieEntity, newMovieType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setPatientType(newMovieType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updatePatientRequest")
    @ResponsePayload
    public UpdatePatientResponse updatePatient(@RequestPayload UpdatePatientRequest request) {
        UpdatePatientResponse response = new UpdatePatientResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        // 1. Find if movie available
        Patient movieFromDB = service.getEntityByTitle(request.getPatientName());

        if (movieFromDB == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Movie = " + request.getPatientName() + " not found");
        } else {

            // 2. Get updated movie information from the request
            movieFromDB.setPatientName(request.getPatientName());
            movieFromDB.setPatientEmail(request.getPatientEmail());
            movieFromDB.setPatientPassword(request.getPatientPassword());
            movieFromDB.setPatientSurname(request.getPatientSurname());
            // 3. update the movie in database

            boolean flag = service.updateEntity(movieFromDB);

            if (flag == false) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getPatientName());
                ;
            } else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }


        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePatientRequest")
    @ResponsePayload
    public DeletePatientResponse deletePatient(@RequestPayload DeletePatientRequest request) {
        DeletePatientResponse response = new DeletePatientResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        boolean flag = service.deleteEntityById(request.getIdPatient());

        if (flag == false) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getIdPatient());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }
}

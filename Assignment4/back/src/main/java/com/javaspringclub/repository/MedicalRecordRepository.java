package com.javaspringclub.repository;

import com.javaspringclub.entity.MedicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Integer> {
}

package com.javaspringclub.repository;

import com.javaspringclub.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Integer> {
    public List<Medication> findAll();

    Medication findByIdmedication(String title);
}

package com.javaspringclub.repository;

import com.javaspringclub.entity.Caregiver;
import com.javaspringclub.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends CrudRepository<Caregiver, Integer> {
    public List<Caregiver> findAll();
    public Caregiver findByIdCaregiver(String id);


    public  Caregiver findCaregiverByIdCaregiver(int idCaregiver);
    public List<Patient> findPatientListByIdCaregiver(int idCaregiver);
}

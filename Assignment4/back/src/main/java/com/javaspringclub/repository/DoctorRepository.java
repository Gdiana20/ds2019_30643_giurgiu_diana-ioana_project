package com.javaspringclub.repository;

import com.javaspringclub.entity.Doctor;
import com.javaspringclub.entity.MovieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Integer> {

    public Doctor findByIdDoctor(String title);
    public Doctor findDoctorByidDoctor(int idDoctor);
    public List<Doctor> findAll();
    public Doctor save(Doctor dc);
}

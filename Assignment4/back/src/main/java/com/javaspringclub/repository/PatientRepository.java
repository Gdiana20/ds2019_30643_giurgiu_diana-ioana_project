package com.javaspringclub.repository;

import com.javaspringclub.entity.Doctor;
import com.javaspringclub.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Integer> {
    public List<Patient> findAll();
    public Patient findByIdPatient(String id);
    //public List<Patient> findAllByCaregiver_idCaregiver(PatientDTO care);

}

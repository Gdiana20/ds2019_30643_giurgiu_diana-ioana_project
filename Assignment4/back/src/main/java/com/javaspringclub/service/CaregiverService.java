package com.javaspringclub.service;

import com.javaspringclub.entity.Caregiver;
import com.javaspringclub.entity.Patient;

import java.util.List;

public interface CaregiverService {

    public Caregiver getEntityById(int id);
    public Caregiver getEntityByTitle(String title);
    public List<Caregiver> getAllEntities();
    public Caregiver addEntity(Caregiver entity);
    public boolean updateEntity(Caregiver entity);
    public boolean deleteEntityById(int id);
}

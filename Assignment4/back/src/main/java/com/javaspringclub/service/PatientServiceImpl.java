package com.javaspringclub.service;

import com.javaspringclub.entity.Patient;
import com.javaspringclub.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PatientServiceImpl implements PatientService{

    private PatientRepository repository;

    public PatientServiceImpl() {

    }

    @Autowired
    public PatientServiceImpl(PatientRepository repository) {
        this.repository = repository;
    }

    @Override
    public Patient getEntityById(int id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Patient getEntityByTitle(String title) {
        return this.repository.findByIdPatient(title);
    }

    @Override
    public List<Patient> getAllEntities() {
        List<Patient> list = new ArrayList<>();
        repository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public Patient addEntity(Patient entity) {
        try {
            return  this.repository.save(entity);
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean updateEntity(Patient entity) {
        try {
            this.repository.save(entity);
            return true;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntityById(int id) {
        try {
            this.repository.deleteById(id);
            return true;
        }catch(Exception e) {
            return false;
        }

    }

}

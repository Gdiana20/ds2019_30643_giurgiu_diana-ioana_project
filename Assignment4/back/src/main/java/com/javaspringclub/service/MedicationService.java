package com.javaspringclub.service;

import com.javaspringclub.entity.Medication;
import com.javaspringclub.entity.MovieEntity;

import java.util.List;

public interface MedicationService {
    public Medication getEntityById(int id);
    public Medication getEntityByTitle(String title);
    public List<Medication> getAllEntities();
    public Medication addEntity(Medication entity);
    public boolean updateEntity(Medication entity);
    public boolean deleteEntityById(int id);
}

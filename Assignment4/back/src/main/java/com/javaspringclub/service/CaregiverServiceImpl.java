package com.javaspringclub.service;

import com.javaspringclub.entity.Caregiver;
import com.javaspringclub.repository.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CaregiverServiceImpl implements CaregiverService{

    private CaregiverRepository repository;

    public CaregiverServiceImpl() {

    }

    @Autowired
    public CaregiverServiceImpl(CaregiverRepository repository) {
        this.repository = repository;
    }

    @Override
    public Caregiver getEntityById(int id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Caregiver getEntityByTitle(String title) {
        return this.repository.findByIdCaregiver(title);
    }

    @Override
    public List<Caregiver> getAllEntities() {
        List<Caregiver> list = new ArrayList<>();
        repository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public Caregiver addEntity(Caregiver entity) {
        try {
            return  this.repository.save(entity);
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean updateEntity(Caregiver entity) {
        try {
            this.repository.save(entity);
            return true;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntityById(int id) {
        try {
            this.repository.deleteById(id);
            return true;
        }catch(Exception e) {
            return false;
        }

    }

}

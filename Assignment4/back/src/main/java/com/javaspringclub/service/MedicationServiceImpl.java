package com.javaspringclub.service;


import com.javaspringclub.entity.Medication;
import com.javaspringclub.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MedicationServiceImpl implements MedicationService {

    private MedicationRepository repository;

    public MedicationServiceImpl() {

    }

    @Autowired
    public MedicationServiceImpl(MedicationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Medication getEntityById(int id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Medication getEntityByTitle(String title) {
        return this.repository.findByIdmedication(title);
    }

    @Override
    public List<Medication> getAllEntities() {
        List<Medication> list = new ArrayList<>();
        repository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public Medication addEntity(Medication entity) {
        try {
            return  this.repository.save(entity);
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean updateEntity(Medication entity) {
        try {
            this.repository.save(entity);
            return true;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntityById(int id) {
        try {
            this.repository.deleteById(id);
            return true;
        }catch(Exception e) {
            return false;
        }

    }



}

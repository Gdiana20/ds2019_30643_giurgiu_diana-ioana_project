package com.javaspringclub.service;

import com.javaspringclub.entity.Doctor;
import com.javaspringclub.entity.MovieEntity;

import java.util.List;

public interface DoctorService {

    public Doctor getEntityById(int id);
    public Doctor getEntityByTitle(String title);
    public List<Doctor> getAllEntities();
    public Doctor addEntity(Doctor entity);
    public boolean updateEntity(Doctor entity);
    public boolean deleteEntityById(int id);
}

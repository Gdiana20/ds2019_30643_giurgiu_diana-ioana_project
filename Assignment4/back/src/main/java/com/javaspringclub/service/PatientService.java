package com.javaspringclub.service;

import com.javaspringclub.entity.Doctor;
import com.javaspringclub.entity.Patient;

import java.util.List;

public interface PatientService {

    public Patient getEntityById(int id);
    public Patient getEntityByTitle(String title);
    public List<Patient> getAllEntities();
    public Patient addEntity(Patient entity);
    public boolean updateEntity(Patient entity);
    public boolean deleteEntityById(int id);
}

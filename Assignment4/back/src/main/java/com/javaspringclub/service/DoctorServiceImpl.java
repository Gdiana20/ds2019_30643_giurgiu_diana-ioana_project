package com.javaspringclub.service;

import com.javaspringclub.entity.Doctor;
import com.javaspringclub.entity.MovieEntity;
import com.javaspringclub.repository.DoctorRepository;
import com.javaspringclub.repository.MovieEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DoctorServiceImpl implements DoctorService {

    private DoctorRepository repository;

    public DoctorServiceImpl() {

    }

    @Autowired
    public DoctorServiceImpl(DoctorRepository repository) {
        this.repository = repository;
    }

    @Override
    public Doctor getEntityById(int id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Doctor getEntityByTitle(String title) {
        return this.repository.findByIdDoctor(title);
    }

    @Override
    public List<Doctor> getAllEntities() {
        List<Doctor> list = new ArrayList<>();
        repository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public Doctor addEntity(Doctor entity) {
        try {
            return  this.repository.save(entity);
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean updateEntity(Doctor entity) {
        try {
            this.repository.save(entity);
            return true;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntityById(int id) {
        try {
            this.repository.deleteById(id);
            return true;
        }catch(Exception e) {
            return false;
        }

    }



}

package com.assignment3.server;
// Creating a Search interface
import com.assignment3.entities.Medication;
import com.assignment3.entities.MedicationPlan;
import com.assignment3.entities.Patient;
import org.graalvm.compiler.lir.sparc.SPARCArithmetic;

import java.awt.*;
import java.rmi.*;
public interface Search extends Remote
{
    // Declaring the method prototype
    public String query(String search) throws RemoteException;
    public MedicationPlan findMedicationPlan(int pacientID) throws RemoteException;
   // public List<Medication> findMedicationListByPacientID(int pacientID) throws RemoteException;
    public Patient findPacientById(int pacientID) throws RemoteException;
    public Medication findMedicationByMediationID(int medicationID) throws RemoteException;
}